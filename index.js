const fs = require('fs');
const path = require('path');

const directoryPath = './data';

const idSet = new Set();

// Loop through each file in the data directory
fs.readdir(directoryPath, function (err, files) {
    if (err) {
        console.log('Error getting directory information.');
    } else {
        files.forEach(function (fileName) {
            const fileContents = fs.readFileSync(path.join(directoryPath, fileName), 'utf8');
            const jsonData = JSON.parse(fileContents);
            console.log(jsonData)
            // const id = jsonData.identifier.id;
            const preRelease = jsonData.preRelease;
            if (!preRelease) {
                // idSet.add(id);
            }
        });

        // Convert set to an array of strings with no duplicates
        const idArray = Array.from(idSet);

        console.log(idArray);
    }
});
